package com.nutthawut.oxgameui;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author OMEN
 */
class Table implements Serializable{

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastCol;
    private int lastRow;

    public Table(Player X, Player O) {
        playerX = X;
        playerO = O;
        currentPlayer = X;
    }

    public Table() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }
    public char getRowCol(int row, int col){
        return table[row][col];
    }
    public boolean setRowCol(int Row, int Col) {
        if(isFinish()) return false;
        if (table[Row][Col] == '-') {
            table[Row][Col] = currentPlayer.getName();
            this.lastRow = Row;
            this.lastCol = Col;
            checkWin();
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    public void checkCol() {
        for (int Row = 0; Row < 3; Row++) {
            if (table[Row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatwinLose();
    }

    private void setStatwinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    public void checkRow() {
        for (int Col = 0; Col < 3; Col++) {
            if (table[lastRow][Col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatwinLose();
    }

    void checkX() {

    }

    void checkDraw() {
        
    }

    public void checkWin() {
        checkRow();
        checkCol();
    }
    public boolean isFinish(){
        return finish;
    }
    public Player getWinner(){
        return winner;
    }
}
